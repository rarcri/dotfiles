execute pathogen#infect()
let g:cpp_attributes_highlight = 1
let g:cpp_member_highlight = 1
set number
syntax on

"Comment color
hi LineNr ctermfg=darkgray | hi comment ctermfg=darkgray ctermbg=black

set tabstop=4
set autoindent
filetype plugin on

" Tabs mapping
map <F5> :tabe
map <F7> :tabp <CR>
map <F8> :tabn <CR>

let g:user_emmet_leader_key=','

call plug#begin('~/.vim/plugged')
call plug#end()
